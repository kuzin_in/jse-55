package ru.kuzin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.kuzin.tm.dto.request.ProjectUpdateByIdRequest;
import ru.kuzin.tm.util.TerminalUtil;

@Component
public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "project-update-by-id";

    @NotNull
    private static final String DESCRIPTION = "Update project by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(getToken());
        request.setId(id);
        request.setName(name);
        request.setDescription(description);
        projectEndpoint.updateProjectById(request);
    }

}