package ru.kuzin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kuzin.tm.dto.model.UserDTO;
import ru.kuzin.tm.dto.request.UserRegistryRequest;
import ru.kuzin.tm.util.TerminalUtil;

@Component
public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-registry";

    @NotNull
    private static final String DESCRIPTION = "registry user";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(getToken());
        request.setLogin(login);
        request.setPassword(password);
        request.setEmail(email);
        @Nullable final UserDTO user = userEndpoint.registryUser(request).getUser();
        showUser(user);
    }

}