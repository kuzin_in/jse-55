package ru.kuzin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.kuzin.tm.dto.request.TaskUpdateByIdRequest;
import ru.kuzin.tm.util.TerminalUtil;

@Component
public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-update-by-id";

    @NotNull
    private static final String DESCRIPTION = "Update task by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(getToken());
        request.setId(id);
        request.setName(name);
        request.setDescription(description);
        taskEndpoint.updateTaskById(request);
    }

}