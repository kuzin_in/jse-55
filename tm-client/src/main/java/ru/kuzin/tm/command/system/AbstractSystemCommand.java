package ru.kuzin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kuzin.tm.api.endpoint.ISystemEndpoint;
import ru.kuzin.tm.api.service.ICommandService;
import ru.kuzin.tm.api.service.IPropertyService;
import ru.kuzin.tm.command.AbstractCommand;

@Component
public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected ICommandService commandService;

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    protected ISystemEndpoint systemEndpoint;

}