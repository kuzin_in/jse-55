package ru.kuzin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.kuzin.tm.dto.request.ApplicationAboutRequest;
import ru.kuzin.tm.dto.response.ApplicationAboutResponse;

@Component
public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private static final String NAME = "about";

    @NotNull
    private static final String DESCRIPTION = "Show development info.";

    @NotNull
    private static final String ARGUMENT = "-a";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final ApplicationAboutRequest request = new ApplicationAboutRequest();
        @NotNull final ApplicationAboutResponse response = systemEndpoint.getAbout(request);

        System.out.println("[ABOUT]");
        System.out.println("AUTHOR: " + response.getName());
        System.out.println("E-MAIL: " + response.getEmail());
        System.out.println();

        System.out.println("[GIT]");
        System.out.println("BRANCH: " + response.getGitBranch());
        System.out.println("COMMIT ID: " + response.getGitCommitId());
        System.out.println("COMMITTER: " + response.getGitCommitterName());
        System.out.println("E-MAIL: " + response.getGitCommitterEmail());
        System.out.println("MESSAGE: " + response.getGitCommitMessage());
        System.out.println("TIME: " + response.getGitCommitTime());
    }

}