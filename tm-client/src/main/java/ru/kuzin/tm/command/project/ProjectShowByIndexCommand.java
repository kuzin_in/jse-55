package ru.kuzin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.kuzin.tm.dto.request.ProjectShowByIndexRequest;
import ru.kuzin.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "project-show-by-index";

    @NotNull
    private static final String DESCRIPTION = "Display project by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectShowByIndexRequest request = new ProjectShowByIndexRequest(getToken());
        request.setIndex(index);
        projectEndpoint.showProjectByIndex(request);
    }

}