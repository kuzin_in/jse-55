package ru.kuzin.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.comparator.CreatedComparator;
import ru.kuzin.tm.comparator.NameComparator;
import ru.kuzin.tm.comparator.StatusComparator;

import java.util.Comparator;

public enum Sort {

    BY_NAME("Sort by name", "name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", "status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", "created", CreatedComparator.INSTANCE);

    @NotNull
    private final String displayName;

    @NotNull
    private final String sortField;

    @NotNull
    private final Comparator<?> comparator;

    @Nullable
    public static Sort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final Sort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    Sort(@NotNull final String displayName, @NotNull final String sortField, @NotNull final Comparator<?> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
        this.sortField = sortField;
    }

    @NotNull
    public Comparator getComparator() {
        return comparator;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    public String getSortField() {
        return sortField;
    }

}